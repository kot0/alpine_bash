FROM alpine

RUN apk add --no-cache bash && sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

CMD ["/bin/bash"]


